package com.example.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.LaptopModel;
import com.example.belajarspring.repository.LaptopRepository;



@Service
@Transactional
public class LaptopService {
	
	
	  @Autowired 
	  private LaptopRepository laptopRepository;
	  
	  public void create(LaptopModel laptopModel) {
		  laptopRepository.save(laptopModel);
	  }
	  
	  public List<LaptopModel> read(){
		  return laptopRepository.findAll();
	  }
	  
	  public void update(LaptopModel laptopModel) {
		  this.laptopRepository.save(laptopModel);
	  }
	  
	  public void delete(String nomorLaptop) {
		  this.laptopRepository.deleteById(nomorLaptop);
		
	}
	 
}
