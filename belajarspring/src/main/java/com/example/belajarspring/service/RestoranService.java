package com.example.belajarspring.service;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.RestoranModel;
import com.example.belajarspring.repository.RestoranRepository;



@Service
@Transactional
public class RestoranService {

		@Autowired
		private RestoranRepository RestoranRepository;
		
		public void create(RestoranModel restoranModel) {
			RestoranRepository.save(restoranModel);
			// method save tdk ada di repository krn berasal dari JpaRepository
		}
		
		public List<RestoranModel> read() {
			return RestoranRepository.findAll();
		}
		
		public List<RestoranModel> readOrderBy() {
			return RestoranRepository.querySelectAllOrderNamaDesc();
			
		}
		
		public List<RestoranModel> readWhereNama(String carinama) {
			return RestoranRepository.querySearchNama(carinama);
			
		}
}
