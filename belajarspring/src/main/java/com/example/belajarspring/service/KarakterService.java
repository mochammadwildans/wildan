package com.example.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.KarakterModel;
import com.example.belajarspring.repository.KarakterRepository;

@Service
@Transactional
public class KarakterService {

	@Autowired
	private KarakterRepository karakterRepository;
	
	public void create(KarakterModel karakterModel) {
		karakterRepository.save(karakterModel);
	}
	
	public void update( KarakterModel karakterModel) {
		this.karakterRepository.save(karakterModel);
	}
	
	public List<KarakterModel> read() {
		return karakterRepository.findAll();
		
	}
	
	public void delete(String nama) {
		this.karakterRepository.deleteById(nama);
	}
}
