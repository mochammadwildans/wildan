package com.example.belajarspring.service;

import java.util.ArrayList;


import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.DosenModel;
import com.example.belajarspring.repository.DosenRepository;

@Service
@Transactional
public class DosenService {

	@Autowired
	private DosenRepository dosenRepository;
	
	// modifier kosong/tdk namaMethod(TipeVariable namaVariabel)
	public void create(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
		// method save tdk ada di repository krn berasal dari JpaRepository
	}
	
	public void update(DosenModel dosenModel) {
		this.dosenRepository.save(dosenModel);
	}
	
	public List<DosenModel> read() {
		return dosenRepository.findAll();
	}
	
	//-------------------------- PADA API -DOSEN ------------------
	
	
	
	public List<DosenModel> readDuaLima() {
		return dosenRepository.queryUsiaDuaLima();
	}
	
	public List<DosenModel> readDuaPuluh(){
		return dosenRepository.queryDuaPuluh();
	}
	
	
	//-------------------------- PADA API -DOSEN ------------------
	
	public List<DosenModel> readOrderNama() {
		return dosenRepository.querySelectAllOrderNamaDesc();
	}
	
	public List<DosenModel> readOrderBy() {
		return dosenRepository.querySelectAllOrderUsiaDesc();
	}
	
	public List<DosenModel> readWhereNama(String kataKunci) {
		return dosenRepository.querySelectAllWhereNama(kataKunci);
	}
	
	
	
	
	public List<DosenModel> readWhereUsiaByOperator(int katakunci_usia, String katakunci_operator, String awalan_nama) {
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		
		
		if (katakunci_operator.equals("=")) {
			dosenModelList = dosenRepository.queryWhereUsiaEqual(katakunci_usia, awalan_nama);
		} else if (katakunci_operator.equals(">")) {
			dosenModelList = dosenRepository.queryWhereUsiaMore(katakunci_usia, awalan_nama);
		} else if (katakunci_operator.equals("<")) {
			dosenModelList = dosenRepository.queryWhereUsiaLess(katakunci_usia, awalan_nama);
		} else if (katakunci_operator.equals(">=")) {
			dosenModelList = dosenRepository.queryWhereUsiaMoreEqual(katakunci_usia, awalan_nama);
		} else if (katakunci_operator.equals("<=")) {
			dosenModelList = dosenRepository.queryWhereUsiaLessEqual(katakunci_usia, awalan_nama);
		} else {
			dosenModelList = dosenRepository.queryWhereUsiaNotEqual(katakunci_usia, awalan_nama);
		}
		
		return dosenModelList;
	}
	
	
	
	
	
	// ----------------------------------------------------------------------
	
	
	
	public DosenModel readByID(String namaID) {
		return dosenRepository.queryWhereID(namaID);
	}
	
	public void delete(String namaDosen) {
		  dosenRepository.deleteById(namaDosen);
	}
}
