package com.example.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.BajuModel;
import com.example.belajarspring.repository.BajuRepository;

@Service
@Transactional
public class BajuService {
	
	@Autowired
	private BajuRepository bajuRepository;
	
	public void create(BajuModel bajuModel) {
		bajuRepository.save(bajuModel);
		
	}
	
	public List<BajuModel> read() {
		return bajuRepository.findAll();
	}
	
	
	
	public List<BajuModel> readBajuB(){
		return bajuRepository.queryBajuB();
	}
	
	
	
	public List<BajuModel> readGws(){
		return bajuRepository.queryGws();
	}

}
