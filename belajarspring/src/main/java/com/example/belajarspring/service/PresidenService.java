package com.example.belajarspring.service;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.PresidenModel;
import com.example.belajarspring.repository.PresidenRepository;

@Service
@Transactional
public class PresidenService {
	
	@Autowired
	private PresidenRepository presidenRepository;
	
	public void create(PresidenModel presidenModel) {
		presidenRepository.save(presidenModel);
		// method save tdk ada di repository krn berasal dari JpaRepository
	}
	
	
	
	public List<PresidenModel> read() {
		return presidenRepository.findAll();
	}
	
	public List<PresidenModel> readKotag() {
		return presidenRepository.queryReadKotag();
	}

}
