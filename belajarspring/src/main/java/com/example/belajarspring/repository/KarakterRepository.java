package com.example.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.belajarspring.model.KarakterModel;

public interface KarakterRepository extends JpaRepository<KarakterModel, String> {

}
