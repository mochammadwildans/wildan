package com.example.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.belajarspring.model.PresidenModel;

public interface PresidenRepository extends JpaRepository<PresidenModel, String>{

	@Query("SELECT P FROM PresidenModel P WHERE P.kotaPresiden LIKE '%G' AND P.usiaPresiden < 70 ")
	List<PresidenModel> queryReadKotag();
}
