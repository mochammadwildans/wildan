package com.example.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.belajarspring.model.BajuModel;

public interface BajuRepository extends JpaRepository<BajuModel, String> {

	@Query("SELECT B FROM BajuModel B WHERE B.namaBaju LIKE 'B%' ")
	List<BajuModel> queryBajuB();
	
	@Query("SELECT B FROM BajuModel B WHERE B.nomorBaju = 20 AND B.namaBaju LIKE '%C%' ORDER By B.namaBaju ASC")
	List<BajuModel> queryGws();
}
