package com.example.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.belajarspring.model.LaptopModel;

public interface LaptopRepository extends JpaRepository<LaptopModel, String> {

}
