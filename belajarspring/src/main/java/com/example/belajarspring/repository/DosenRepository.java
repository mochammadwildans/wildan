package com.example.belajarspring.repository;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.belajarspring.model.DosenModel;

public interface DosenRepository extends JpaRepository<DosenModel, String>{

	// Ini bukan SQL tapi HQL (Hibernate Query Language)
	// HQL itu query yg bisa dijalankan pada hibernate
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen DESC")
	List<DosenModel> querySelectAllOrderNamaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen ASC")
	List<DosenModel> querySelectAllOrderNamaAsc();
	
	
	
	//-------------------------- PADA API -DOSEN ------------------
	
	
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen > 25")
	List<DosenModel> queryUsiaDuaLima();
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen = 20")
	List<DosenModel> queryDuaPuluh();
	
	
	
	//-------------------------- PADA API -DOSEN ------------------
	
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen DESC")
	List<DosenModel> querySelectAllOrderUsiaDesc();
	
	
//	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen ASC")
//	List<DosenModel> querySelectAllOrderUsiaAsc();
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1%")
	List<DosenModel> querySelectAllWhereNama(String kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1%")
	List<DosenModel> queryGetNama(String namaDosen);
	
	
	
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen =?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryWhereUsiaEqual(int katakunci_usia, String awalan_nama );
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen >?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryWhereUsiaMore(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen <?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryWhereUsiaLess(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen >=?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryWhereUsiaMoreEqual(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen <=?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryWhereUsiaLessEqual(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen !=?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryWhereUsiaNotEqual(int katakunci_usia, String awalan_nama);
	
	
	
	
	
	
	//Untuk Pembanding Huruf Depan
	
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1% ")
	DosenModel queryWhereID(String namaID);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1% ")
	DosenModel deleteById(int namaID);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1% ")
	DosenModel save(String namaID);
}
