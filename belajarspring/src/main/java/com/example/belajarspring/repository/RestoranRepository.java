package com.example.belajarspring.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.belajarspring.model.RestoranModel;

public interface RestoranRepository extends JpaRepository<RestoranModel, String>{

	// Ini bukan SQL tapi HQL (Hibernate Query Language)
	// HQL itu query yg bisa dijalankan pada hibernate
	@Query("SELECT R FROM RestoranModel R ORDER BY R.namaMenu DESC")
	List<RestoranModel> querySelectAllOrderNamaDesc();
	
	@Query("SELECT R FROM RestoranModel R ORDER BY R.namaMenu ASC")
	List<RestoranModel> querySelectAllOrderNamaAsc();
	
	@Query("SELECT R FROM RestoranModel R WHERE R.namaMenu LIKE %?1%")
	List<RestoranModel> querySearchNama(String carinama);
}
