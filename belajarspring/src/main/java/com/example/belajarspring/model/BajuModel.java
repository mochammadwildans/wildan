package com.example.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_BAJU")
public class BajuModel {
	
	@Id
	@Column(name="NMR_BAJU")
	private int nomorBaju;
	
	@Column(name="NM_BAJU")
	private String namaBaju;

	public int getNomorBaju() {
		return nomorBaju;
	}

	public void setNomorBaju(int nomorBaju) {
		this.nomorBaju = nomorBaju;
	}

	public String getNamaBaju() {
		return namaBaju;
	}

	public void setNamaBaju(String namaBaju) {
		this.namaBaju = namaBaju;
	}
	
	

}
