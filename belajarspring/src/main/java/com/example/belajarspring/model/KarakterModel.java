package com.example.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_KRKTER")
public class KarakterModel {
	
	@Id
	@Column(name="NM_KRKTER")
	private String nama;
	
	@Column(name="LVL_KRKTER")
	private int level;
	
	@Column(name="STATUS_KRKTER")
	private String status;
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
