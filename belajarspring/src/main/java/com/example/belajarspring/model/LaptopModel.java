package com.example.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_LAPTOP")
public class LaptopModel {
	
	@Id
	@Column(name="KLM_NOMOR")
	private String nomorLaptop;
	
	@Column(name="KLM_Nama")
	private String namaLaptop;
	
	@Column(name="KLM_SPESIFIKASI")
	private String spekLaptop;
	

	

	public String getNomorLaptop() {
		return nomorLaptop;
	}

	public void setNomorLaptop(String nomorLaptop) {
		this.nomorLaptop = nomorLaptop;
	}

	public String getNamaLaptop() {
		return namaLaptop;
	}

	public void setNamaLaptop(String namaLaptop) {
		this.namaLaptop = namaLaptop;
	}
	
	public String getSpekLaptop() {
		return spekLaptop;
	}

	public void setSpekLaptop(String spekLaptop) {
		this.spekLaptop = spekLaptop;
	}
	
}
