package com.example.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_BOTOL")
public class BotolModel {
	
	
	
	// BELUM SELESAI
	
	
	
	@Id
	@Column(name="NM_BTL")
	private String namaBotol;
	
	@Column(name="SP_BTL")
	private String spekBotol;
	
	
	public String getNamaBotol() {
		return namaBotol;
	}
	public void setNamaBotol(String namaBotol) {
		this.namaBotol = namaBotol;
	}
	public String getSpekBotol() {
		return spekBotol;
	}
	public void setSpekBotol(String spekBotol) {
		this.spekBotol = spekBotol;
	}

	
}
