package com.example.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_PRESIDEN")
public class PresidenModel {
	
	@Id
	@Column(name="NO_PRES")
	private int noPresiden;
	
	@Column(name="NM_PRES")
	private String namaPresiden;
	
	@Column(name="KT_PRES")
	private String kotaPresiden;
	
	@Column(name="USIA_PRES")
	private int usiaPresiden;
	
	
	
	public int getNoPresiden() {
		return noPresiden;
	}
	public void setNoPresiden(int noPresiden) {
		this.noPresiden = noPresiden;
	}
	public String getNamaPresiden() {
		return namaPresiden;
	}
	public void setNamaPresiden(String namaPresiden) {
		this.namaPresiden = namaPresiden;
	}
	public String getKotaPresiden() {
		return kotaPresiden;
	}
	public void setKotaPresiden(String kotaPresiden) {
		this.kotaPresiden = kotaPresiden;
	}
	public int getUsiaPresiden() {
		return usiaPresiden;
	}
	public void setUsiaPresiden(int usiaPresiden) {
		this.usiaPresiden = usiaPresiden;
	}
	
	

}
