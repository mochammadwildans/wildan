package com.example.belajarspring.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import com.example.belajarspring.model.RestoranModel;
import com.example.belajarspring.service.RestoranService;


@Controller
public class RestoranController {

	
	@Autowired
	private RestoranService restoranService;
	
	@RequestMapping(value="tambah_restoran")
	public String menuIsiRestoran() {
		String html = "restoran/isi_restoran";
		return html;
	}
	
	
	@RequestMapping(value="hasil_satu_restoran")
	public String menuSatuDosen(HttpServletRequest request, Model model) {
		
		String namaMenu = request.getParameter("tambah_nama_Menu");
		int harga = Integer.valueOf(request.getParameter("tambah_harga"));
				
		RestoranModel restoranModel = new RestoranModel();
		restoranModel.setNamaMenu(namaMenu);
		restoranModel.setHarga(harga);

		// Transaksi simpan C create
		restoranService.create(restoranModel);
	
		model.addAttribute("restoranModel", restoranModel);
		
		String html = "restoran/satu_restoran";
		return html;
	}
	
	
	@RequestMapping(value="hasil_banyak_restoran")
	public String menuBanyakRestoran(Model model) {
		
		List<RestoranModel> restoranModelList = new ArrayList<RestoranModel>();
		
		restoranModelList= restoranService.read();
		model.addAttribute("restoranModelList", restoranModelList);
		
		String html ="restoran/banyak_restoran";
		return html;
	}
	@RequestMapping(value="cari_restoran")
	public String menuCariRestoran( Model model) {
		
		List<RestoranModel> restoranModelList = new ArrayList<RestoranModel>();
		restoranModelList = restoranService.readOrderBy();
		model.addAttribute("restoranModelList", restoranModelList);
		
		String html ="restoran/cari_restoran";
		return html;
	}
	
	@RequestMapping(value="proses_cari_restoran")
	public String prosesCariRestoran(HttpServletRequest request, Model model) {
		String carinama = request.getParameter("carinama");
		
		List<RestoranModel> restoranModelList = new ArrayList<RestoranModel>();
		restoranModelList = restoranService.readWhereNama(carinama);
		model.addAttribute("restoranModelList", restoranModelList);
		
		String html ="restoran/cari_restoran";
		return html;
	}


}
