package com.example.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrisaktiController {

	@RequestMapping(value="menusatu")
	private String methodPanggilSatu() {
		String html ="/trisakti/tiga";
		return html;
		
	}
	
	
	
	@RequestMapping(value="menudua")
	private String methodPanggilHasil(HttpServletRequest request, Model model) {
		String nama = request.getParameter("daftar_nama");
		String tempat = request.getParameter("daftar_tempat");
		String tanggal = request.getParameter("daftar_tanggal");
		String jk = request.getParameter("daftar_jk");
		String agama = request.getParameter("daftar_agama");
		String telp = request.getParameter("daftar_telp");
		String hp = request.getParameter("daftar_hp");
		String negara = request.getParameter("daftar_negara");
		String nik = request.getParameter("daftar_nik");
		String goldar = request.getParameter("daftar_goldar");
		String jakarta = request.getParameter("daftar_jakarta");
		String anak = request.getParameter("daftar_anak");
		String nikah = request.getParameter("daftar_nikah");
		
		System.out.println(nama);
		System.out.println(tempat);
		System.out.println(tanggal);
		System.out.println(jk);
		System.out.println(agama);
		System.out.println(telp);
		System.out.println(hp);
		System.out.println(negara);
		System.out.println(nik);
		System.out.println(goldar);
		System.out.println(jakarta);
		System.out.println(anak);
		System.out.println(nikah);
		
		
		model.addAttribute("hasil_nama", nama);
		model.addAttribute("hasil_tempat", tempat);
		model.addAttribute("hasil_tanggal", tanggal);
		model.addAttribute("hasil_jk", jk);
		model.addAttribute("hasil_agama", agama);
		model.addAttribute("hasil_telp", telp);
		model.addAttribute("hasil_hp", hp);
		model.addAttribute("hasil_negara", negara);
		model.addAttribute("hasil_nik", nik);
		model.addAttribute("hasil_goldar", goldar);
		model.addAttribute("hasil_jakarta", jakarta);
		model.addAttribute("hasil_anak", anak);
		model.addAttribute("hasil_nikah", nikah);
		
		String html ="/trisakti/lima";
		return html;
		
	}
	
	
	
	
	}
	
