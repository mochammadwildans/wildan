package com.example.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.belajarspring.model.KarakterModel;
import com.example.belajarspring.service.KarakterService;

@RestController
@RequestMapping("api/karakter")
public class KarakterApi {
	
	@Autowired
	KarakterService karakterService;
	
	@GetMapping("/get")
	@ResponseStatus(code= HttpStatus.OK)
	public List<KarakterModel> get(){
		List<KarakterModel> karakterModelList = new ArrayList<KarakterModel>();
		karakterModelList = this.karakterService.read();
		return karakterModelList;
	}
	
	@PostMapping("/post")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Map<String, Object> post (@RequestBody KarakterModel karakterModel){
		this.karakterService.create(karakterModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", Boolean.TRUE);
		return map;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code= HttpStatus.OK)
	public Map<String, Object> get (@RequestBody KarakterModel karakterModel){
		this.karakterService.update(karakterModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("SUCCESS", Boolean.TRUE);
		return map;
	}
	
	@DeleteMapping("/delete/{nama}")
	@ResponseStatus(code= HttpStatus.GONE)
	public Map<String, Object> delete (@PathVariable String nama){
		karakterService.delete(nama);
		
		
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("SUCCESS", Boolean.TRUE);
	return map;
		
		
	}
	

}
