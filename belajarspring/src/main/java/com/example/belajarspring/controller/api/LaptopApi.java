package com.example.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.belajarspring.model.LaptopModel;
import com.example.belajarspring.service.LaptopService;

@RestController
@RequestMapping("api/laptop")
public class LaptopApi {

	@Autowired
	private LaptopService laptopService;
	
	
	@PostMapping("/post")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Map<String, Object> post(@RequestBody LaptopModel laptopModel){
		this.laptopService.create(laptopModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code= HttpStatus.OK)
	public List<LaptopModel> get(){
		List<LaptopModel> laptopModels = new ArrayList<LaptopModel>();
		laptopModels = this.laptopService.read();
		return laptopModels;
	}
	
	
	@PutMapping("/put")
	@ResponseStatus(code= HttpStatus.OK)
	public Map<String, Object> put (@RequestBody LaptopModel laptopModel){
		this.laptopService.update(laptopModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("SUCCESS", Boolean.TRUE);
		return map;
	}
	
	@DeleteMapping("/delete/{nomorLaptop}")
	@ResponseStatus(code= HttpStatus.GONE)
	public Map<String, Object> delete(@PathVariable String nomorLaptop){
		this.laptopService.delete(nomorLaptop);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("SUCCESS", Boolean.TRUE);
		return map;
	}
}
