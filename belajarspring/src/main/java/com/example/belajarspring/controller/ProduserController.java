package com.example.belajarspring.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProduserController {
	
	@RequestMapping(value="produser/linkdaftar")
	public String doDaftar() {
		String html ="produser/daftar";
		return html;
	}
	
	@RequestMapping(value="produser/linkhasil")
	public String doHasil(HttpServletRequest request, Model model) {
		String nama = request.getParameter("daftar_nama");
		String alamat = request.getParameter("daftar_alamat");
		String umur = request.getParameter("daftar_umur");
		
		
		System.out.println(nama);
		System.out.println(alamat);
		System.out.println(umur);
		
		model.addAttribute("hasil_nama", nama);
		model.addAttribute("hasil_alamat", alamat);
		model.addAttribute("hasil_umur", umur);
		
		
		String html ="produser/hasil";
		return html;
	}
	

}
