package com.example.belajarspring.controller;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.belajarspring.model.BajuModel;
import com.example.belajarspring.service.BajuService;

@Controller
public class BajuController {
	
	@Autowired
	private BajuService bajuService;
	
	@RequestMapping(value="tambah_baju")
	public String menuTambahBaju(Model model) {
		String html ="baju/tambah_baju";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_baju")
	public String prosesTambahBaju(HttpServletRequest request, Model model) {
		int nomorBaju = Integer.valueOf(request.getParameter("tambah_nomor"));
		String namaBaju  = request.getParameter("tambah_nama");
		
		BajuModel bajuModel = new BajuModel();
		bajuModel.setNomorBaju(nomorBaju);
		bajuModel.setNamaBaju(namaBaju);
		
		bajuService.create(bajuModel);
		
		model.addAttribute("bajuModel", bajuModel);
		
		
		
		String html ="baju/satu_baju";
		return html;
	}
	
	@RequestMapping(value="hasil_banyak_baju")
	public String prosesBanyakBaju(Model model) {
		
		List<BajuModel> bajuModelList = new ArrayList<BajuModel>();
		bajuModelList = bajuService.read();
		
		model.addAttribute("bajuModelList", bajuModelList);
		
		String html ="baju/banyak_baju";
		return html;
	}

}
