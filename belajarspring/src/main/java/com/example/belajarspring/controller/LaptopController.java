package com.example.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.belajarspring.model.LaptopModel;
import com.example.belajarspring.service.LaptopService;

@Controller
public class LaptopController {

	
	  @Autowired private LaptopService laptopService;
	 

	@RequestMapping(value = "tambah_laptop")
	public String menuTambahLaptop(Model model) {
		String Html = "laptop/tambah_laptop";
		return Html;
	}

	@RequestMapping(value="proses_satu_laptop")
	public String menuSatuLaptop(HttpServletRequest request, Model model) {
		String nomorLaptop =request.getParameter("tambah_nomor");
		String namaLaptop = request.getParameter("tambah_nama");
		String spekLaptop = request.getParameter("tambah_spek");
		
		LaptopModel laptopModel = new LaptopModel();
		laptopModel.setNomorLaptop(nomorLaptop);
		laptopModel.setNamaLaptop(namaLaptop);
		laptopModel.setSpekLaptop(spekLaptop);
		
		laptopService.create(laptopModel);
		
		model.addAttribute("laptopModel", laptopModel);
		
		
		String Html ="laptop/satu_laptop";
		return Html;
	}
	
	@RequestMapping(value="banyak_laptop")
	public String menuBanyakLaptop(Model model) {
		
		List<LaptopModel> laptopModelList = new ArrayList<LaptopModel>();
		laptopModelList=laptopService.read();
		
		model.addAttribute("laptopModelList", laptopModelList);
		
		String Html="laptop/banyak_laptop";
		return Html;
	}
		
}
