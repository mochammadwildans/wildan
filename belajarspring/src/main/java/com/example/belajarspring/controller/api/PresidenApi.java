package com.example.belajarspring.controller.api;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.belajarspring.model.PresidenModel;
import com.example.belajarspring.service.PresidenService;

@RestController
@RequestMapping("/api/presiden")
public class PresidenApi {
	
	@Autowired
	private PresidenService presidenService;
	
	@PostMapping("/post")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody PresidenModel presidenModel){
		this.presidenService.create(presidenModel);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("pesan", "Selamat Udah Bisa PULANG CEPAT");
		return map;
		
	}

	@GetMapping("/get")
	@ResponseStatus(code= HttpStatus.OK)
	public List<PresidenModel> getApi(){
		List<PresidenModel> presidenModelList = new ArrayList<PresidenModel>();
		presidenModelList = this.presidenService.read();
		return presidenModelList;
		
	}
	
	
	
	@GetMapping("/kotag")
	@ResponseStatus(code= HttpStatus.OK)
	public List<PresidenModel> usiaEqualKotag(){
		List<PresidenModel> presidenModelList = new ArrayList<PresidenModel>();
		presidenModelList = this.presidenService.readKotag();
		return presidenModelList;
	}

	
}
