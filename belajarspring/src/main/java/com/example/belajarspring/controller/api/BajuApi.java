package com.example.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.belajarspring.model.BajuModel;
import com.example.belajarspring.service.BajuService;

@RestController
@RequestMapping("api/baju")
public class BajuApi {
	
	@Autowired
	private BajuService bajuService;
	
	@PostMapping("/post")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Map<String, Object> post(@RequestBody BajuModel bajuModel){
		this.bajuService.create(bajuModel);
		
		Map<String, Object> map= new HashMap<String, Object>();
		map.put("success", Boolean.TRUE);
		return map;
	}
	
	@GetMapping("/hurufb")
	@ResponseStatus(code= HttpStatus.OK)
	public List<BajuModel> hurufEqualB(){
		List<BajuModel> bajuModelList = new ArrayList<BajuModel>();
		bajuModelList = this.bajuService.readBajuB();
		return bajuModelList;
	}
	
	@GetMapping("/gws")
	@ResponseStatus(code=HttpStatus.OK)
	public List<BajuModel> gunWithoutSnipper(){
		List<BajuModel> bajuModels = new ArrayList<BajuModel>();
		bajuModels = this.bajuService.readGws();
		return bajuModels;
	}
	

}
