package com.example.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.belajarspring.model.DosenModel;
import com.example.belajarspring.service.DosenService;

@RestController
@RequestMapping("/api/dosenApi")
public class DosenApi {
	
	@Autowired
	private DosenService dosenService;
	
	
	@GetMapping("/get")
	@ResponseStatus(code= HttpStatus.OK)
	public List<DosenModel> getApi(){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.read();
		return dosenModelList;
		
	}
	
	//INI Yang URL usiadualima Adalah Bentuk Get Untuk Select Data Usia Kurang Dari 25
	
	@GetMapping("/usiadualima")
	@ResponseStatus(code= HttpStatus.OK)
	public List<DosenModel> usiaLessThan(){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readDuaLima();
		return dosenModelList;
		
	}
	
	@GetMapping("/usiaduapuluh")
	@ResponseStatus(code= HttpStatus.OK)
	public List<DosenModel> usiaEqualDuaPuluh(){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readDuaPuluh();
		return dosenModelList;
	}

	@PostMapping("/post")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody DosenModel dosenModel){
		this.dosenService.create(dosenModel);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("pesan", "Selamat Udah Bisa PULANG CEPAT");
		return map;
		
	}
	
	@PutMapping("/put")
	@ResponseStatus(code= HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody DosenModel dosenModel){
		this.dosenService.update(dosenModel);
		
	Map<String, Object>map = new HashMap<String, Object>();
	map.put("success", Boolean.TRUE);
	map.put("Alert", "Data Berhasil diupdate");
	return map;
	}
	
	@DeleteMapping("/delete/{namaDosen}")
	@ResponseStatus(code=HttpStatus.GONE)
	public Map<String, Object> deleteApi (@PathVariable String namaDosen ){
		dosenService.delete(namaDosen);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("SELAMAT", Boolean.TRUE);
		map.put("ACC", "Data Anda Atas Nama " + namaDosen+ " Telah Dihapus");
		return map;
	}
}
